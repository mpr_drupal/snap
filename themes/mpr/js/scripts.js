

jQuery(function(){
	
	jQuery(document).ready(function () {
		window.currentState = '';
		window.currentStateName = '';
		window.currentData = 'data-def'
		window.dvtop= '';
		
		jQuery(".scale-list").hide();
		jQuery(".scale100-50").show();
		jQuery(".scalehhdef").show();	
		jQuery(".scalehidef").show();	
		jQuery(".scalesadef").show();	
		jQuery(".scaleprdef").show();		
		
	/* Options for highcharts */
			if(typeof Highcharts !== 'undefined'){
				Highcharts.setOptions({
    			lang: {
    		thousandsSep: ",",
        numericSymbols: null
    }
				});
			}
/* Set class of each state path.  The class determines background color. */		 
 jQuery('path').each(function (index, value) { 
 		 	  var dval = jQuery(this).attr('data-def');
 		 	 
 		 	  SetClass(dval,this);
 		 	
		});
		
		 // jQuery('body.path-usamap h1').html("SNAP Participation Rates by State, <span class='active-title'>All Eligible People</span>");
 		 // jQuery('body.path-snap-state-characteristics h1').html("Percentage of Participating SNAP Households with <span class='active-title'>Children</span>");
	
		jQuery('.state-rate').hide();
	
});
	
	/** function to run when user clicks on a state */
	jQuery(".us-state").click(function () {
		jQuery(".us-state").removeClass('active-state');
		jQuery('.state-rate').show();
		jQuery('.one-state').show();
		jQuery(this).addClass('active-state');
	
 		 	  window.currentState = jQuery(this).attr('data-abbr');
 		 
 		 	  SetTopRates();

	});
	
	/** function to run when user selects state from state dropdown list */
	jQuery('.state-select').change(function() {
			var sval = jQuery(this).val();

		jQuery(".us-state").removeClass('active-state');
		jQuery('.state-rate').show();
		jQuery('.one-state').show();
		jQuery('.us-state-' + sval).addClass('active-state');

 		 	  window.currentState = sval;
 		 
 		 	  SetTopRates();
				
		});
	
	/** Function to run when user clicks map button **/
	jQuery(".map-trigger").click(function () {
		
		var newhead = jQuery(this).attr('data-head');
		var newcat = jQuery(this).attr('data-cat');
		
		/* If the button has a data-vtop attribute to be used to determine which scale to use, then get that value */
		window.dvtop = '';
		if (jQuery(this)[0].hasAttribute("data-vtop")) {
			window.dvtop = jQuery(this).attr('data-vtop');
			jQuery(".scale-list").hide();
			if (window.dvtop== '73') {
			 			jQuery(".scale70-10").show();		 			
			 		}
			 		else if (window.dvtop == '100') {
			 			jQuery(".scale100-50").show();	
			 		}
			 	  else jQuery(".scale" + window.dvtop).show();
		};

		window.currentData = 'data-'+newcat; 	
		
		jQuery(".map-trigger").removeClass('active-button');	
		jQuery(this).addClass('active-button');

		jQuery('body.path-usamap h1').html("SNAP Participation Rates by State, <strong>" + newhead + " </strong>");
 		jQuery('body.path-snap-state-characteristics h1').html("Percentage of Participating SNAP Households with <strong>" + newhead + " <strong>");
 		jQuery('body.path-snap-household-state-averages h1').html("SNAP household state averages for <strong>" + newhead + " </strong> (FY 2017)")
 		 
 		 SetTopRates();
 		 
 		jQuery('path').each(function (index, value) { 
 		 	  var dval = jQuery(this).attr('data-'+newcat);	 	 
 		 	  SetClass(dval,this);				
		 });
	});
	
/*  Used for more options drop down list */
		jQuery('#more-options-select').change(function() {
			var sval = jQuery(this).val();
			var sid = jQuery(this ).attr('id');
			var stext=jQuery('#'+sid + ' option:selected' ).text();
			if(stext == 'View data table'){
				jQuery('.'+sval).show();
				jQuery('#'+sid + ' option:selected' ).text('Hide data table');
				jQuery('#'+sid ).val('0').change();
				
			}
			else if(stext == 'Hide data table'){
				jQuery('.'+sval).hide();
				jQuery('#'+sid + ' option:selected' ).text('View data table');
				jQuery('#'+sid ).val('0').change();
			}  
			else if (sval !="0"){
				window.location.href=sval;
				jQuery('#'+sid).val('0').change();
			}
			else return false;
		});
	
	
	function SetClass(dval, elem) {
		
		 /* Remove any active classes on map before setting new ones. */
			jQuery(elem).removeClass (function (index, className) {
         return (className.match (/(^|\s)active-map-\S+/g) || []).join(' ');
      }); 	
			 	/* vtop variable is set in view header custom text area (see usamap views).  This value is used to determine which map scale to use */
			 	var vtop = jQuery('#vtop').html();
			 	
			 	/** If there is a value for vtop from map button, override value from custom text area **/
			 	if (window.dvtop != '') {
			 		vtop = window.dvtop;
			 		
			 	}
			 	
			 	/** If there isn't a value for the state, then set class to standard N/A style **/
			 	if(dval == "") {
			 		jQuery(elem).addClass('active-map-data-na');
			 	}
			 	else {
			 	/** Scale arrays **/
			 	var theArray3 = [2.5,2.3,2.1,1.9,1.7];
			 	var theArray10 = [100,90,80,70,60 ];
			 	var theArray13 = [70,60,50,40,30,20];	
			  var theArray11 = [60,50,40,30,20,10];	
	
			  var theArrayprwork = [100.95,90,85,80,75]	 
			  var theArray21 = [21,19,17,15,13,11,9];				
				var theArray42 = [90,85,80,75,70,65,60,55,45];				
				var theArray600 = [575,299,279,259,239,219,199];
				var theArray1000 = [1200,1000,950,900,850,800,750,600];
				var theArrayhhold = [40,34,28,22,16];
				var theArrayhhdef = [60,53,46,39,32];
				var theArrayhhadult = [30,25,20,15,10];
				var useArray = theArray10;
				if (vtop == '60') useArray = theArray11; 
				else if (vtop == 90) useArray = theArray42;
					else if (vtop == 73) useArray = theArray13;
					else if (vtop == '1200' || vtop == 'sainc') useArray = theArray1000;
						else if (vtop == 'sadef') useArray = theArray600;
						else if (vtop == 'sacert') useArray = theArray21;
						else if (vtop == 'sasize') useArray = theArray3;
						else if (vtop == 'hhold') useArray = theArrayhhold;
						else if (vtop == 'hhdef') useArray = theArrayhhdef;
						else if (vtop == 'hhadult') useArray = theArrayhhadult;
					  else if (vtop == 'hidef') useArray = [42,36,32,28,24,14];
					  else if (vtop == 'hizero') useArray = [29,24,21,18,15,10,5];
					  else if (vtop == 'hissi') useArray = [35,28,26,24,22,18,13];
					  else if (vtop == 'hitanf') useArray = [20,10,5,4,3,2,0];
						else if (vtop == 'hhdis') useArray = [45,30,25,20,15];
						else if (vtop == 'prdef') useArray =  [100,95,90,85,80,75]	;
						else if (vtop == 'prwork') useArray =  [100,90,80,75,70,65]	;
						else if (vtop == 'prold') useArray =  [70,55,50,45,40,35,30]	;
						else if (vtop == 'sapov') useArray =  [90,74,69,64,59,54]	;
				var closest = null;

				jQuery.each( useArray, function(){
  					if (closest == null || (Math.abs(this - dval) < Math.abs(closest - dval))&& this >= dval) {
    				closest = this;
    				
  				}
				});
				
		/*** Note: because of overlap in scales, needed to modify class names to distinguish **/
				if (vtop == 60) closest =  closest + 1;
    		else if (vtop == 90) closest =  closest + 2;
    			else if (vtop == 73) closest =  closest + 3;		
    				else if (vtop == 3) closest =  closest;
    			var sclosest = "active-map-"+closest.toString().replace(".","-")
    			if (isNaN(vtop) ) sclosest =  sclosest.toString() + vtop;
    				
				jQuery(elem).addClass(sclosest);
			}
	}
	function SetTopRates() {
		/** Get data values **/
		var uval = jQuery('.all-states').attr(window.currentData);
		var sval = jQuery('.us-state-'+ window.currentState).attr(window.currentData);
		var nval = jQuery('.us-state-'+ window.currentState).attr('data-name');
		
		/* 
		The vtop and vtype variables are set in view header custom text area (see usamap views).  
		These values are used to determine how to customize state and national value text 
		Not all views have a vtype
		*/
			var vtop = jQuery('#vtop').html();
			var vtype  = "";
			/** If there is a value for vtop from map button, override value from custom text area **/
			if (window.dvtop != '') {
			 		vtop = window.dvtop;			 		
			}
			if(jQuery('#val-type').length !== 0) {
			    vtype = "&nbsp;" + jQuery('#val-type').html();
		  }
		  
		  
		var funitchar = ""; 
		var bunitchar = "%";
		if (vtop > 100 || vtop == 'sainc' || vtop == 'sadef') {
			funitchar = "$";
			bunitchar = "";
		} 		
		if (vtop == 'sacert') {
			funitchar = "";
			bunitchar = " months";
		}
		if (vtop == 'sasize') {
			funitchar = "";
			bunitchar = " people";
		} 
		if (sval == "" && nval != "United States") {
			funitchar = "";
			bunitchar = " ";
			sval = "Not applicable";
		} 

		
	
		   jQuery('.all-states').html("National" + vtype + ": <span class='active-val'>"+ funitchar + uval + bunitchar + "</span>");
		   jQuery('.one-state').html(nval + vtype +  ": <span class='active-val'>"+ funitchar + sval + bunitchar +"</span><span style='font-size:.75em;'>&nbsp;&nbsp;<a href='" + window.location.pathname + "'>Reset</a></span>");
		 
		 
	}
});